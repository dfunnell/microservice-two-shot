# Wardrobify

Team:

* David Funnell - Hats microservice
* Tommy Mai - Shoes microservice


## Start Up Instructions

 To run this application, fork and clone the main branch of this project and run the following commands in your terminal within the projects PWD.

    docker volume create pgdata
    docker-compose build
    docker-compose up

After successfully building and starting your containers, create bin and location data with the wardrobe API to be able to create new hats within the hats microservice and create new shoes within the shoes microservice.

## Design
Below is a diagram of the app architecture. It shows our 3 microservices running within docker and their port locations to your local.
![Diagram of the App Architecture](diagram_of_architecture.png)


## Shoes microservice

Models:

Shoes has manufacturer, model name, color, URL for a picture, and the wardrobe bin where it is located. 

BinVo shows the wardrobe bin where it exist and is listed as a
foreign key in the shoes model. 

It is created by polling the Wardrobe api for the bins. 

## Hats microservice

The Hats microservice is run on http://localhost:8090 on your loal machine.

### API Help
Below are a list of RESTful api endpoints that the Hats microservice uses.

To get a list of all of the Hats:
GET: http://localhost:8090/api/hats/

    The response code should be 200 OK if successful. The response should look as shown below.

    {
	"hats": [
		{
        "href": "/api/hats/28/",
        "fabric": "Wool",
        "style": "5-Panel",
        "color": "Blue",
        "picture_url": "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/6-9-1646672159.jpg?crop=1.00xw:1.00xh;0,0&resize=1200:*",
        "location": {
            "closet_name": "Closet Location 1",
            "id": 1,
            "section_number": 1,
            "shelf_number": 1,
            "import_href": "/api/locations/1/"
        },
        "id": 28
		},
        .....additional items shown here...
        ]
    }


To create a Hat:
POST: http://localhost:8090/api/hats/

    below is the correct JSON BODY formatting for POST;
    {
        "fabric": "Cotton",
        "style": "5-Panel",
        "color": "Carbon",
        "picture_url": "https://content.backcountry.com/images/items/1200/BLD/BLD00W7/CB.jpg",
        "location": "1"
    }

    "location": "1" is representing the id of the LocationVO you would like to select.

    The response code should be 200 OK if successful. The response should look as shown below.

    {
        "href": "/api/hats/41/",
        "fabric": "Cotton",
        "style": "5-Panel",
        "color": "Carbon",
        "picture_url": "https://content.backcountry.com/images/items/1200/BLD/BLD00W7/CB.jpg",
        "id": 41,
        "location": {
            "closet_name": "Closet Location 1",
            "id": 1,
            "section_number": 1,
            "shelf_number": 1,
            "import_href": "/api/locations/1/"
            }
    }

    If you have have inserted a location id that does not exist, you will recieve a response code of 400 BAD REQUEST. The response should look as shown below.

    {
	    "message": "Invalid Closet Name"
    }


To delete a Hat:
DELETE: http://localhost:8090/api/hats/#/

    Insert the hat id in place of # in the endpoint to correctly delete the hat of your choice.

    The response code should be 200 OK if successful. The response should look as shown below.

    {
	    "deleted": "true"
    }

    If you have have inserted a hat id that does not exist, you will recieve a response code of 400 BAD REQUEST. The response should look as shown below.

    {
	    "message": "Does not exist, Can't Delete"
    }


***** Additionally included functionality

To get details for a specific hat;
GET: http://localhost:8090/api/hats/#/

    Insert the hat id in place of # in the endpoint to correctly see details on the hat of your choice.

    The response code should be 200 OK if successful. The response should look as shown below.

    {
        "href": "/api/hats/28/",
        "fabric": "Wool",
        "style": "5-Panel",
        "color": "Blue",
        "picture_url": "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/6-9-1646672159.jpg?crop=1.00xw:1.00xh;0,0&resize=1200:*",
        "id": 28,
        "location": {
            "closet_name": "Closet Location 1",
            "id": 1,
            "section_number": 1,
            "shelf_number": 1,
            "import_href": "/api/locations/1/"
            }
    }


To see a list of all hats at a given location;
GET: http://localhost:8090/api/location/#/hats/

    Insert the location id in place of # in the endpoint to correctly see a list of hat at your location of choice.

    The response code should be 200 OK if successful. The response should look as shown below.

    {
	"hats": [
		{
        "href": "/api/hats/32/",
        "fabric": "Wool",
        "style": "Safari",
        "color": "Red",
        "picture_url": "https://content.backcountry.com/images/items/1200/BUR/BURZ8AO/FEL.jpg",
        "location": {
            "closet_name": "Closet Location 3!",
            "id": 3,
            "section_number": 3,
            "shelf_number": 3,
            "import_href": "/api/locations/3/"
            },
        "id": 32
		},
        .....additional items shown here...
	    ]
    }


To see a list of all currently polled location value objects within the hats microservice;
GET: http://localhost:8090/api/locationsvo/

    {
	"locations": [
		{
        "closet_name": "Closet Location 1",
        "id": 1,
        "section_number": 1,
        "shelf_number": 1,
        "import_href": "/api/locations/1/"
		},
        .....additional items shown here...
	    ]
    }


Below are a list of RESTful api endpoints and that the Hats microservice requires within the wardrobe API. A POST of a location is initially required to create a Post of a Hat as a Hat requires a location for posting.

To create a Location:
POST: http://localhost:8100/api/locations/

    below is the correct JSON BODY formatting for POST;

    {
        "closet_name": "Closet Location 4",
        "section_number": "4",
        "shelf_number": "4"
    }

    The response code should be 200 OK if successful. The response should look as shown below.

    {
        "href": "/api/locations/4/",
        "id": 4,
        "closet_name": "Closet Location 4",
        "section_number": "4",
        "shelf_number": "4"
    }


To see a list of all locations;
GET: http://localhost:8100/api/locations/

    The response code should be 200 OK if successful. The response should look as shown below.

    {
	"locations": [
		{
        "href": "/api/locations/1/",
        "id": 1,
        "closet_name": "Closet Location 1",
        "section_number": 1,
        "shelf_number": 1
		},
        .....additional items shown here...
	    ]
    }


To delete a given location;
DELETE: http://localhost:8100/api/locations/#/

    Insert the location id in place of # in the endpoint to correctly delete the location of your choice. BE AWARE - If you delete a location in the wardrobe API that does not mean it will be deleted in the Hats microservice polled data.

    The response code should be 200 OK if successful. The response should look as shown below.

    {
        "id": null,
        "closet_name": "Closet Location 4",
        "section_number": 4,
        "shelf_number": 4
    }

    If you have have inserted a location id that does not exist, you will recieve a response code of 400 BAD REQUEST. The response should look as shown below.

    {
	"message": "Does not exist"
    }


### Below are the models within the Hats microservice:
#### Hat
    fabric          <- of CharField type    (max length of 200 char)
    style           <- of CharField type    (max length of 200 char)
    color           <- of CharField type    (max length of 200 char)
    picture_url     <- of URLField type     (optional)
    location        <- Foreignkey linked to LocationVO

fabric, style...etc are fields of the model.


#### LocationVO
    closet_name         <- of CharField type    (max length of 100 char)
    section_number      <- of PositiveSmallIntegerField type
    shelf_number        <- of PositiveSmallIntegerField type
    import_href         <- of CharField type    (max length of 200 char)(Will be Unique)

LocationVO is a value object that is storing polled data from the wardrobe locations API. We are polling this data to keep all one-to-many relationship data within the microservice that is using it. We poll this data from the endpoint "http://wardrobe-api:8000/api/locations/" at a frequency of 60 seconds.
