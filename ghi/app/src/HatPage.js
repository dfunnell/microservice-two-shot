import { NavLink } from 'react-router-dom';
import { Outlet } from "react-router-dom";


function HatsPage() {
    return (
        <>
            <div className="px-4 py-2 my-2 text-center">
                <h1 className="display-5 fw-bold">Hats!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                        Keep track of your hats!
                    </p>
                    <NavLink className="nav-link" to="/hats/new">Add Hats</NavLink>
                    <NavLink className="nav-link" to="/hats/list">See All Hats</NavLink>
                </div>
            </div>
            <Outlet />
        </>
    );
}

export default HatsPage;
