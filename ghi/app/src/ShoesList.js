import { NavLink } from 'react-router-dom';

function ShoesList(props) {
  const deleteShoe = async (id) => {
    const shoesUrl = `http://localhost:8080/api/shoes/${id}`;
    const fetchConfig = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      window.location.reload();
    }
  };

  return (
    <div>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.model_name}</td>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.color}</td>
                <td>
                  <img src={shoe.picture_url} className="card-img-top" max-height={200} alt="Hopefully a shoe" />
                </td>
                <td>
                  <button onClick={(e) => deleteShoe(shoe.id)} className='btn btn-secondary'>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <button>
        <NavLink className="nav-link" to="/shoes/new">Create</NavLink>
      </button>
    </div>
  );
}

export default ShoesList;