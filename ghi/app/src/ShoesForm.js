import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            modelName: '',
            color: '',
            pictureUrl: '',
            bins: []
        };
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleModelNameChange = this.handleModelNameChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
    }

    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState({ modelName: value });
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({ bin: value });
    }

    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({ pictureUrl: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.model_name = data.modelName;
        data.picture_url = data.pictureUrl;
        delete data.pictureUrl;
        delete data.modelName;
        delete data.bins;
        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log('newShoe:', newShoe);

            const cleared = {
                manufacturer: '',
                model_name: '',
                picture_url: '',
                bin: '',
            }
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new shoe</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleModelNameChange} required type="text" name="model_name" id="model_name" className="form-control" />
                                <label htmlFor="model_name">Model Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureUrlChange} required type="text" name="picture_url" id="picture_url" className="form-control" />
                                <label htmlFor="picture_url">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleBinChange} required id="bin" name="bin" className="form-select">
                                    <option value="">Choose a bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            <option key={bin.id} value={bin.href}>
                                                {bin.bin_number}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShoeForm;
