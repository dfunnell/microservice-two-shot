import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';


import HatsList from './HatsList';
import HatsForm from './HatsForm';
import HatsPage from './HatPage';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />}/>

          <Route path="hats" element={<HatsPage/>}>
            <Route path="list" element={<HatsList/>}/>
            <Route path="new" element={<HatsForm/>}/>
          </Route>
          <Route path="shoes">
            <Route path="/shoes/" element={<ShoesList shoes={props.shoes} />} />
          </Route>
          <Route path="shoes">
            <Route path="new" element={<ShoesForm shoes={props.shoes} />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>

  );
}

export default App;
